#!/bin/bash
set -e
export AWS_PROFILE=pf-sandbox-developer

disableDistribution() {
    local CLOUDFRONT_DISTRIBUTION_ID="$1"
    local ETAG="$2"
    local DISTRIBUTION_CONFIG_FILE=$(mktemp /tmp/distribution-config.XXXXXX.json)

    aws cloudfront get-distribution-config --id ${CLOUDFRONT_DISTRIBUTION_ID} --query DistributionConfig | jq .Enabled=false >${DISTRIBUTION_CONFIG_FILE}
    aws cloudfront update-distribution --id ${CLOUDFRONT_DISTRIBUTION_ID} --if-match ${ETAG} --distribution-config file://${DISTRIBUTION_CONFIG_FILE} >/dev/null
    aws cloudfront wait distribution-deployed --id ${CLOUDFRONT_DISTRIBUTION_ID}
}

getETag() {
    local CLOUDFRONT_DISTRIBUTION_ID="$1"

    aws cloudfront get-distribution-config --id ${CLOUDFRONT_DISTRIBUTION_ID} --query ETag --output text
}

deleteDistribution() {
    local CLOUDFRONT_DISTRIBUTION_ID="$1"
    local ETAG="$2"

    aws cloudfront delete-distribution --id ${CLOUDFRONT_DISTRIBUTION_ID} --if-match ${ETAG}
}

destroy() {
    local BUCKET_NAME="$1"
    local CLOUDFRONT_DISTRIBUTION_ID="$2"

    #get etag
    local ETAG=$(getETag ${CLOUDFRONT_DISTRIBUTION_ID})
    #disable distribution
    disableDistribution ${CLOUDFRONT_DISTRIBUTION_ID} ${ETAG}
    #get etag
    local ETAG=$(getETag ${CLOUDFRONT_DISTRIBUTION_ID})
    #delete distribution
    deleteDistribution ${CLOUDFRONT_DISTRIBUTION_ID} ${ETAG}

    aws s3 rb --force s3://${BUCKET_NAME}
}

DISTRIBUTION_ID=$(aws cloudfront list-distributions --profile pf-sandbox-developer --query DistributionList.Items[0].Id --output text)

BUCKET_NAME=$(aws s3api list-buckets --query Buckets[0].Name --output text)

destroy "${BUCKET_NAME}" "${DISTRIBUTION_ID}"
